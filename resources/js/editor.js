// Creating modelist.
const modelist = ace.require("ace/ext/modelist");
// importing plugins.
ace.require("ace/ext/emmet");
ace.require("ace/ext/language_tools");
// Creating ace js editor in "editor" id.
const editor = ace.edit("editor");

/**
 * Reads config.json and applys it to ace js.
 */
function loadSettings() {
    editor.setTheme("ace/theme/gruvbox");
    const d = getSettings();
    d.then(data => {
        console.log("Settings found!");
        console.log(data); 
        editor.setOptions(data)
    });
}

/**
 * Sets the mode of the editor by the file.
 * @param {string} dir direction of the file
 */
function setMode(dir) {
    const mode = modelist.getModeForPath(dir).mode;
    editor.session.setMode(mode);
    console.log(mode);
}

/**
 * Sets the current editing file.
 * @param {string} data File content
 */
function setCurrentFile(data) {
    editor.session.setValue(data);
}

/**
 * Gets current file data and returns it.
 * @returns {string} Data of the file
 */
function getCurrentFileData() {
    return editor.getValue();
}

/**
 * Searches for a word and replaces it.
 * @param {string} find Word to find
 * @param {string} replace Replacing word
 */
function findAndReplace(find, replace) {
    editor.find(find);
    editor.replace(replace);
}

async function getSettings() {
    const d = await Neutralino.filesystem.readFile('./config.json');
    return JSON.parse(d)
}

