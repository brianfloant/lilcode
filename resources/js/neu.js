/**
 * Opens a file and retursn its data.
 * @param {string} direction direction of the file
 * @returns {string} file data
 */
async function readFile(direction) {
    return Neutralino.filesystem.readFile(direction);
}
/**
 * Open a dialog for getting file dir.
 * @param {string} windowTitle Title that window will have
 * @returns {Array<string>} Files selected
 */
async function openFileDialog(windowTitle) {
    return Neutralino.os.showOpenDialog(windowTitle);
}
/**
 * Writes or creates a file.
 * @param {string} direction File direction
 * @param {string} data Data to add to the file
 */
async function writeFile(direction, data) {
    Neutralino.filesystem.writeFile(direction, data);
}