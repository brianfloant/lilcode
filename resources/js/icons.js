/**
 * All the icons are NOT made by me.
 * ALl the icons are made by FREEPIK!
 * Thanks to FREEPIK!
 */

//////////////////////////////////////////
//              Variables.
//////////////////////////////////////////
const icondir = 'icons/files/';
const icontype = '.png';
//////////////////////////////////////////
//              Variables.
//////////////////////////////////////////
/**
 * Main function to add icon and title.
 * @param {string} dir The file name duh.
 */
function addIconTitle(dir) {
    O('#header').empty();
    const icon = document.createElement('img');
    icon.src = `icons/files/${getFileType(dir)}.png`;
    icon.width = '16';
    icon.height = '16';
    const name = document.createElement('p');
    name.innerText = getFileName(dir);
    name.classList.add('file-name');
    O('#header').append(icon);
    O('#header').append(name);
}
/**
 * Just gets the type of a file.
 * @param {string} filename The file name again duh.
 * @returns string.
 */
function getFileType(filename) {
    const type = filename.match(/\.[a-zA-Z]*/)[0];
    console.log(type);
    const name = type.split(/\./)[1];
    console.log(name);
    return name;
}
function getFileName(dir) {
    return dir.match(/[a-zA-Z]*\.[a-zA-Z]*/);
}
/**
 * Icon generator.
 */
class Icon {
    /**
     * Icon constructor
     * @param {string} type type of the file.
     */
    constructor(type) {
        this.icon = document.createElement('img');
        this.icon.src = `icons/files/${type}.png`;
        this.icon.width = '16';
        this.icon.height = '16';
    }
    /**
     * Just returns the icon.
     * @returns Icon element.
     */
    get() {
        return this.icon;
    }
}
/**
 * Paragraph maker.
 */
class Paragraph {
    /**
     * Constructor.
     * @param {string} text Whole name of the file.
     */
    constructor(text) {
        this.paragraph = document.createElement('p');
        this.paragraph.innerText = text;
        this.paragraph.classList.add('file-name');
    }
    /**
     * Just returns the element.
     * @returns paragraph.
     */
    get() {
        return this.paragraph;
    }
}