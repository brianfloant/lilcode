window.myApp = {
    setTray: () => {
        if(NL_MODE != "window") {
            console.log("INFO: Tray menu is only available in the window mode.");
            return;
        }
        let tray = {
            icon: "/resources/icons/trayIcon.png",
            menuItems: [
                {id: "VERSION", text: "Get version"},
                {id: "SEP", text: "-"},
                {id: "QUIT", text: "Quit"}
            ]
        };
        Neutralino.os.setTray(tray);
    },
    onWindowClose: () => {
        Neutralino.app.exit();
    }
};

// Dragg Window
const header = document.getElementById('header');
let dragg = false;
let posX, posY;

header.onmousedown = function (e) {
    posX = e.pageX;
    posY = e.pageY;
    dragg = true;
}
header.onmouseup = function (e) {
    dragg = false;
}
header.onmousemove = function (e) {
    if (dragg) Neutralino.window.move(e.screenX - posX, e.screenY - posY);
}

// Initialize native API communication. This is non-blocking
// use 'ready' event to run code on app load.
// Avoid calling API functions before init or after init.
Neutralino.init(); 

Neutralino.events.on("windowClose", myApp.onWindowClose);
Neutralino.events.on("ready", () => {
    if(NL_OS != "Darwin") { // TODO: Fix https://github.com/neutralinojs/neutralinojs/issues/615
        window.myApp.setTray();
    }
    loadSettings();

    
});