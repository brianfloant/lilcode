
# Lil Code

A minimalist code editor for people like me, which prioritizes minimalism and lightness.
It is designed to only use the keyboard as a VIM, but being easier to use and configure. Also to be very light with the Ram and CPU to finally be able to play Lol while editing my code.


## Install
Just download the file for your OS.

If you are on Windows, you might get a blank white screen. The reason for this is, accessing localhost from a UWP context is disabled by default. Run the following command with administrative privileges on the command prompt to fix this.

```powershell
CheckNetIsolation.exe LoopbackExempt -a -n="Microsoft.Win32WebViewHost_cw5n1h2txyewy"
```


## How To Use
Lil Code works by key combinations, for now the master key is 'alt': \
Open a file: 'alt' and 'f', \
Save current file: 'alt' and 's', \
Create a new file: 'alt' and 'n', \
Close the editor: 'alt' and 'q'. 
## Configuration
It is very easy, you just have to enter the file 'config.json' and edit the options to your liking.
## Comparison
This comparison was thrown on my pc, it is only for the Ram which is important for me, since I do not have a super pc. Therefore, my need is for it to be as light as possible.
| PC |  Vs Code  | Lil Code | 
|:-----|:--------:|------:|
| Ram   | 391 | 61 |

## Screenshot

![Lil Code Screenshot](https://i.ibb.co/G2C9zF6/LilCode.png)

## Things To Add
- [ ]  An elegant way to view and open directories without compromising minimalism.
- [ ]  Customize keyboard commands.
- [ ]  A fancy way to see a keyboard command helper when you press the master key alone.
- [ ]  More themes.

## Special Thanks
This could be done thanks to the help of:
- [Neutralino.js](https://neutralino.js.org "@embed")
- [Ace.js](https://ace.c9.io/ "@embed")
- [KeyboardJS](https://robertwhurst.github.io/KeyboardJS/ "@embed")
- [Sweetalert2](https://sweetalert2.github.io/ "@embed")
- [Icons by Freepik](https://www.flaticon.com/authors/freepik "@embed")
