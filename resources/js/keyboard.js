// Variable that contains file direccion.
let CurrentFileDir = '';
// Vabriable that contians file data.
let CurrentFileData = '';

////////////////////////////////////////////////////////
//                    Functions
////////////////////////////////////////////////////////

/**
 * Opens a dialog to get file path and data then sets mode and editing file.
 * @param {Event} event Event from keyboard binding
 */
async function obtainingFileAndSet(event) {
    event.preventDefault();
    const dialog = openFileDialog('Select your file');
    dialog.then(d => {
        CurrentFileDir = d[0];
        addIconTitle(CurrentFileDir);
        CurrentFileData =  readFile(CurrentFileDir);
        setMode(CurrentFileDir);
        CurrentFileData.then(f => {
            setCurrentFile(f);
        })
    });
}

/**
 * Saves the current file.
 * @param {Event} event Event from keyboard binding
 */
async function saveCurrentFile(event) {
    event.preventDefault();
    CurrentFileData = getCurrentFileData();
    writeFile(CurrentFileDir, CurrentFileData);
    Swal.fire({
        title: '¡Saved!',
        icon: 'success',
        timer: 1500,
        background: '#1D2021',
        color: '#eadab5',
        showConfirmButton: false
    });
}

/**
 * Creates a new empty file.
 * @param {Event} event Event from keyboard binding
 */
async function createNewFile(event) {
    event.preventDefault();
    const dialog = await Neutralino.os.showSaveDialog('Create a file');
    writeFile(dialog, '');
    Swal.fire({
        title: '¡Created!',
        icon: 'success',
        timer: 1500,
        background: '#1D2021',
        color: '#eadab5',
        showConfirmButton: false
    });
}
/**
 * Just closes the window.
 * @param {Event} event Event from keyboard binding
 */
function closeBind(event) {
    event.preventDefault();
    Neutralino.app.exit();
}
////////////////////////////////////////////////////////
//                    Functions
////////////////////////////////////////////////////////

////////////////////////////////////////////////////////
//                    Bindings
////////////////////////////////////////////////////////
// Binding for open a new file.
keyboardJS.bind('alt > f', obtainingFileAndSet);
// Binding for save the current file.
keyboardJS.bind('alt > s', saveCurrentFile);
// Binding for create a new file.
keyboardJS.bind('alt > n', createNewFile);
//Binding for closing window
keyboardJS.bind('alt > q', closeBind);
////////////////////////////////////////////////////////
//                    Bindings
////////////////////////////////////////////////////////
